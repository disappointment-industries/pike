package utils

import (
	"gitlab.com/MikeTTh/env"
	"log"
	"os/exec"
)

func RunFFmpeg(params []string) (*exec.Cmd, error) {
	params = removeEmptyArray(params)
	cmd := exec.Command(env.String("FFMPEG_PATH", "/usr/bin/ffmpeg"), params...)
	err := cmd.Run()
	if err != nil {
		return nil, err
	}
	return cmd, nil
}

func StartFFmpeg(params []string) (*exec.Cmd, error) {
	params = removeEmptyArray(params)
	cmd := exec.Command(env.String("FFMPEG_PATH", "/usr/bin/ffmpeg"), params...)
	err := cmd.Start()
	if err != nil {
		return nil, err
	}
	return cmd, nil
}

// TODO: Set log to file or something?
func RunFFmpegWithOutput(params []string) (*exec.Cmd, string, error) {
	params = removeEmptyArray(params)
	cmd := exec.Command(env.String("FFMPEG_PATH", "/usr/bin/ffmpeg"), params...)
	out, err := cmd.CombinedOutput()
	log.Println(string(out))
	if err != nil {
		return nil, string(out), err
	}
	return cmd, string(out), nil
}

func removeEmptyArray(arr []string) []string {
	var out []string
	for _, s := range arr {
		if s == "" {
			continue
		}
		out = append(out, s)
	}
	return out
}
