package multipart

import (
	"gitlab.com/MikeTTh/env"
	"io"
	"mime/multipart"
	"net/http"
	"os"
)

func SendingMultipart(name string) error {
	url := env.String("CONTROLLER_HOST", "http://localhost:8081") + "/worker/ts"

	r, w := io.Pipe()
	m := multipart.NewWriter(w)
	go func() {
		defer w.Close()
		defer m.Close()
		part, err := m.CreateFormFile("file", name)
		if err != nil {
			return
		}
		file, err := os.Open(name)
		if err != nil {
			return
		}
		defer file.Close()
		if _, err = io.Copy(part, file); err != nil {
			return
		}
	}()
	_, err := http.Post(url, m.FormDataContentType(), r)
	if err != nil {
		return err
	}
	return nil
}
