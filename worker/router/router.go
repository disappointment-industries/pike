package router

import (
	"github.com/gin-gonic/gin"
	"worker/handlers"
)

func GetRouter() *gin.Engine {
	router := gin.Default()
	router.MaxMultipartMemory = 1024 << 20 // 1024 MiB
	router.POST("/transcode", handlers.MultiPartHandler)
	router.POST("/ffmpeg", handlers.FFmpegHandler)

	return router
}
