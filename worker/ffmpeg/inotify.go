package ffmpeg

import (
	"context"
	"github.com/illarion/gonotify/v2"
	"log"
	"os"
	"worker/multipart"
)

func GoNotifyWatch(dir string, ctx context.Context) {
	watcher, err := gonotify.NewDirWatcher(ctx, gonotify.IN_CREATE|gonotify.IN_CLOSE, dir)
	if err != nil {
		log.Fatalln(err)
	}

	for {
		select {
		case event := <-watcher.C:
			//log.Printf("Event: %s\n", event)

			if event.Mask&gonotify.IN_CLOSE_WRITE != 0 {
				log.Printf("File closed: %s\n", event.Name)

				err := multipart.SendingMultipart(event.Name)
				if err != nil {
					log.Println(err)
				}

				log.Println("removing transcoded file")
				err = os.Remove(event.Name)
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}
