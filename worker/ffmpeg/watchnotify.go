package ffmpeg

import (
	"github.com/fsnotify/fsnotify"
	"log"
	"math"
	"os"
	"sync"
	"time"
	"worker/multipart"
)

func Watch(path string) (*fsnotify.Watcher, error) {

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	go watchLoop(watcher)

	err = watcher.Add(path)
	if err != nil {
		return watcher, err
	}

	return watcher, nil
}

func watchLoop(w *fsnotify.Watcher) {
	var (
		// Wait 1000ms for new events; each new event resets the timer.
		waitFor = 1000 * time.Millisecond * 2

		// Keep track of the timers, as path → timer.
		mu     sync.Mutex
		timers = make(map[string]*time.Timer)

		// Callback we run.
		processFile = func(event fsnotify.Event) {
			err := multipart.SendingMultipart(event.Name)
			if err != nil {
				log.Println(err)
			}

			err = os.Remove(event.Name)
			if err != nil {
				log.Println(err)
			}

			mu.Lock()
			delete(timers, event.Name)
			mu.Unlock()
		}
	)

	for {
		event, ok := <-w.Events
		if ok {
			if !event.Has(fsnotify.Create) && !event.Has(fsnotify.Write) {
				continue
			}

			mu.Lock()
			t, ok := timers[event.Name]
			mu.Unlock()

			// No timer yet, so create one.
			if !ok {
				t = time.AfterFunc(math.MaxInt64, func() { processFile(event) })
				t.Stop()

				mu.Lock()
				timers[event.Name] = t
				mu.Unlock()
			}

			// Reset the timer for this path, so it will start from 100ms again.
			t.Reset(waitFor)

		} else { // Channel was closed (i.e. Watcher.Close() was called).
			return
		}
	}
}
