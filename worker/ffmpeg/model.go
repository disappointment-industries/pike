package ffmpeg

import (
	"github.com/google/uuid"
	"os/exec"
)

type Command struct {
	//Name string    `json:"name"`
	Args []string  `json:"args"`
	Uuid uuid.UUID `json:"uuid"`
}

type FFmpegOutput struct {
	Output string `json:"output"`
}

var FFmpegProcesses map[string]*exec.Cmd

func Init() {
	FFmpegProcesses = make(map[string]*exec.Cmd)
}
