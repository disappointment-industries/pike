package ffmpeg

import (
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"strings"
)

func ProcessFFmpeg(params string, uuid string) {
	argsArray := strings.Split(params, " ")
	go func() {
		_, output, err := utils.RunFFmpegWithOutput(argsArray)

		if err != nil {
			log.Println(output)
			log.Fatalln(err)
		}

		//TODO: check if proccess really ended and sigterm if not, maybe defer?

		//url := env.String("CONTROLLER_HOST", "http://localhost:8081") + "/ffmpeg/stop/" + uuid
		//
		//_, err = http.Post(url, "application/json", nil)
		//if err != nil {
		//	log.Println(err)
		//}
	}()
}
