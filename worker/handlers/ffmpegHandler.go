package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"net/http"
	"worker/ffmpeg"
)

func FFmpegHandler(context *gin.Context) {
	var command ffmpeg.Command

	err := context.ShouldBindJSON(&command)
	if err != nil {
		context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	_, output, err := utils.RunFFmpegWithOutput(command.Args)
	//TODO: check if proccess really ended and sigterm if not, maybe defer?
	if err != nil {
		log.Println(output)
		log.Println(err)
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"output": output, // it's maybe not necessary TODO
			"error":  err,
		})
		return
	}
	context.JSON(http.StatusOK, ffmpeg.FFmpegOutput{Output: output})
}
