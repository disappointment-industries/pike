package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"net/http"
	"strings"
	"worker/ffmpeg"
)

func MultiPartHandler(context *gin.Context) {
	ffmpegParams := context.PostForm("ffmpegParams")
	uuid := context.PostForm("uuid")

	file, _ := context.FormFile("file")

	// Upload the file to specific dst.
	dir := env.String("WORKER_RECEIVE_PATH", "/tmp/pike/worker/received")
	err := utils.CreateDirectory(dir)
	if err != nil {
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		log.Fatalln(err)
	}

	absoluteFilePath := dir + "/" + file.Filename

	err = context.SaveUploadedFile(file, absoluteFilePath)
	if err != nil {
		log.Println(err)
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ffmpegParams = strings.Replace(ffmpegParams, "replaceable_file_name", absoluteFilePath, 1)

	transcodedDir := env.String("WORKER_TRANSCODE_PATH", "/tmp/pike/worker/transcoded")

	ffmpegParams = strings.Replace(ffmpegParams, "replaceable_hls_file_name_with_id/", transcodedDir+"/", 1)
	ffmpegParams = strings.Replace(ffmpegParams, "replaceable_m3u8_file_name", transcodedDir+"/"+uuid, 1)

	ffmpeg.ProcessFFmpeg(ffmpegParams, uuid)

	context.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}
