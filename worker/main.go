package main

import (
	"context"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"worker/ffmpeg"
	"worker/router"
)

func main() {
	dir := env.String("WORKER_TRANSCODE_PATH", "/tmp/pike/worker/transcoded")
	err := utils.CreateDirectory(dir)
	if err != nil {
		log.Fatalln(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go ffmpeg.GoNotifyWatch(dir, ctx)

	r := router.GetRouter()
	r.Run(":" + env.String("PORT", "8082"))
}
