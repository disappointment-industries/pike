package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/pike/utils"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

type Command struct {
	//Name string    `json:"name"`
	Args []string  `json:"args"`
	Uuid uuid.UUID `json:"uuid"`
}

type FFmpegOutput struct {
	Output string `json:"output"`
}

type GinError struct {
	ErrStr string `json:"error"`
}

func main() {
	logFileLocation := env.String("DUMMY_LOG_FILE_LOCATION", "/tmp/dummy-ffmpeg")
	LogFile := logFileLocation

	logFile, err := os.OpenFile(LogFile, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)

	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	log.SetOutput(logFile)

	log.SetFlags(log.Lshortfile | log.LstdFlags)

	command := Command{
		Args: os.Args[1:],
		Uuid: uuid.New(),
	}

	log.Println(command.Args)

	transcode := false

	for _, arg := range command.Args {
		if strings.Contains(arg, "analyzeduration") {
			transcode = true
			break
		}
	}

	if !transcode {
		_, output, err := utils.RunFFmpegWithOutput(command.Args)
		if err != nil {
			log.Println(output)
			log.Fatalln(err)
		}
		fmt.Println(output)
		log.Println(output)
		return
	}

	controllerUrl := env.String("CONTROLLER_URL", "http://localhost:8081")
	content, _ := json.Marshal(command)

	resp, err := http.Post(controllerUrl+"/ffmpeg/start", "application/json", bytes.NewReader(content))

	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {

		//
		ch := make(chan struct{})
		<-ch
	} else {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		if len(body) > 0 {
			var errorOutput GinError
			err := json.Unmarshal(body, &errorOutput)
			if err != nil {
				log.Fatalln(err)
			}

			fmt.Println(errorOutput.ErrStr)
			log.Fatalln(errorOutput.ErrStr)
		}
	}
}
