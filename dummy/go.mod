module dummy

go 1.20

replace gitlab.com/disappointment-industries/pike/utils => ../utils

require (
	github.com/eiannone/keyboard v0.0.0-20220611211555-0d226195f203
	github.com/google/uuid v1.3.0
	github.com/nsf/termbox-go v1.1.1
	gitlab.com/MikeTTh/env v0.0.0-20230128220800-07dab96401a3
	gitlab.com/disappointment-industries/pike/utils v0.0.0-00010101000000-000000000000
)

require (
	github.com/mattn/go-runewidth v0.0.9 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
)
