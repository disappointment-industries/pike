package router

import (
	"controller/handler"
	"github.com/gin-gonic/gin"
)

func GetRouter() *gin.Engine {
	router := gin.Default()
	ffGroup := router.Group("/ffmpeg")
	ffGroup.POST("/start", handler.StartFFmpeg)
	ffGroup.POST("/stop/:id", handler.StopFFmpeg)
	/* // TODO optional pause, continue, if implemented, worker must implement too
	ffGroup.POST("/pause/:id", nil)
	ffGroup.POST("/continue/:id", nil)
	*/

	workerGroup := router.Group("/worker")
	workerGroup.POST("/ts", handler.ReceivedTsFromWorker)

	return router
}
