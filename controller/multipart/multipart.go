package multipart

import (
	"gitlab.com/MikeTTh/env"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
)

func SendingMultipart(file, uuid, params string) error {
	log.Println("starting multipart function with " + uuid)
	url := env.String("WORKER_HOST", "http://localhost:8082") + "/transcode"

	r, w := io.Pipe()
	m := multipart.NewWriter(w)
	go func() {
		log.Println("started multipart multithread function with " + uuid)
		defer w.Close()
		defer m.Close()
		m.WriteField("ffmpegParams", params)
		m.WriteField("uuid", uuid)
		part, err := m.CreateFormFile("file", file)
		if err != nil {
			return
		}
		file, err := os.Open(file)
		if err != nil {
			return
		}
		defer file.Close()
		if _, err = io.Copy(part, file); err != nil {
			return
		}
		log.Println("closed mutlipart multithread function with " + uuid)
	}()
	log.Println("try to post " + uuid)
	_, err := http.Post(url, m.FormDataContentType(), r)
	if err != nil {
		return err
	}
	return nil
}
