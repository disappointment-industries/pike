package handler

import (
	"controller/ffmpeg"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"net/http"
)

func ReceivedTsFromWorker(context *gin.Context) {
	file, _ := context.FormFile("file")

	dir := env.String("CONTROLLER_RECEIVE_PATH", "/config/transcodes")
	err := utils.CreateDirectory(dir)
	if err != nil {
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		log.Fatalln(err)
	}

	var fileName string
	var exist bool

	if fileName, exist = ffmpeg.TsNameCollection[file.Filename]; !exist {
		log.Println(file.Filename + " not exist in TsNameCollection")
	}

	var absoluteFilePath string

	if fileName == "" {
		absoluteFilePath = dir + "/" + file.Filename
	} else {
		absoluteFilePath = dir + "/" + fileName
	}

	err = context.SaveUploadedFile(file, absoluteFilePath)
	if err != nil {
		log.Println(err)
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	context.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}
