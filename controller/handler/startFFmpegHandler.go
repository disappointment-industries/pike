package handler

import (
	"controller/ffmpeg"
	"controller/multipart"
	"github.com/gin-gonic/gin"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"net/http"
	"strings"
)

func StartFFmpeg(context *gin.Context) {

	var command ffmpeg.Command

	err := context.ShouldBindJSON(&command)
	if err != nil {
		errorFunc(context, http.StatusBadRequest, err)
		return
	}

	// Parse FFmpeg params
	params, err := ffmpeg.ParseFFmpegArgs(command.Args)
	if err != nil {
		errorFunc(context, http.StatusInternalServerError, err)
		return
	}

	replacedArgs := replaceThings(command.Args, params)

	shouldSlice, err := ffmpeg.ShouldSlice(params.Filename + params.Extension)
	if err != nil {
		errorFunc(context, http.StatusInternalServerError, err)
		return
	}

	if shouldSlice {
		//create slice path
		targetPath := env.String("SLICE_TARGET_FOLDER", "/tmp/pike/slices") + "/" + params.Id

		err := utils.CreateDirectory(targetPath)
		if err != nil {
			errorFunc(context, http.StatusInternalServerError, err)
			return
		}

		channel := make(chan ffmpeg.TSChan)
		go ffmpeg.CalcNewTsFileNamesInOrder(channel)

		//WatchNotify() // elkuldeni workernek
		//TODO: MUST call defer watch.Close()
		go ffmpeg.GoNotifyWatch(targetPath, command.Uuid.String(), replacedArgs, params, channel)
		//watcher, err := ffmpeg.Watch(targetPath, command.Uuid.String(), replacedArgs, params, channel)
		//if err != nil {
		//	errorFunc(context, http.StatusInternalServerError, err)
		//	return
		//}
		//
		//ffmpeg.WatchNotifyProcesses[command.Uuid.String()] = watcher

		//Slice()
		ffmpeg.Slice(params, targetPath, command.Uuid.String())
	} else {
		// elkuldeni workernek
		err := multipart.SendingMultipart(params.Filename+params.Extension, command.Uuid.String(), replacedArgs)
		if err != nil {
			errorFunc(context, http.StatusInternalServerError, err)
			return
		}
	}
}

func errorFunc(context *gin.Context, status int, err error) {
	log.Println(err)
	context.AbortWithStatusJSON(status, gin.H{"error": err.Error()})
	return
}

func replaceThings(args []string, params *ffmpeg.FFmpegParams) string {

	var appendedArgs string

	for _, arg := range args {
		appendedArgs += arg + " "
	}
	appendedArgs = strings.Replace(appendedArgs, params.Filename+params.Extension, "replaceable_file_name", -1)
	appendedArgs = strings.Replace(appendedArgs, params.HlsFileName, "replaceable_hls_file_name_with_id/"+params.Id+"%d.ts", -1)
	appendedArgs = strings.Replace(appendedArgs, params.M3u8, "replaceable_m3u8_file_name.m3u8", -1)

	return appendedArgs
}
