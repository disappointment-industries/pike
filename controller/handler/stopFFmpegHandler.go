package handler

import (
	"controller/ffmpeg"
	"github.com/gin-gonic/gin"
)

func StopFFmpeg(context *gin.Context) {
	uuid := context.Param("uuid")

	ffmpeg.WatchNotifyProcesses[uuid].Close()

	//TODO: remove the process from ffmpeg.WatchNotifyProcesses

	//TODO: Somehow stop dummy process
}
