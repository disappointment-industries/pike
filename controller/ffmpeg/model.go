package ffmpeg

import (
	"github.com/fsnotify/fsnotify"
	"github.com/google/uuid"
	"os/exec"
)

type Command struct {
	//Name string    `json:"name"`
	Args []string  `json:"args"`
	Uuid uuid.UUID `json:"uuid"`
}

type FFmpegOutput struct {
	Output string `json:"output"`
}

type FFmpegParams struct {
	Filename    string
	HlsTime     float64
	HlsFileName string
	Id          string
	SeekStart   string
	Extension   string
	M3u8        string
}

type TSChan struct {
	SliceName  string
	JellyfinId string
	Params     *FFmpegParams
}

var WatchNotifyProcesses map[string]*fsnotify.Watcher
var FFmpegProcesses map[string]*exec.Cmd
var TsNameCollection map[string]string
var TsNameCount map[string]int

func Init() {
	WatchNotifyProcesses = make(map[string]*fsnotify.Watcher)
	FFmpegProcesses = make(map[string]*exec.Cmd)
	TsNameCollection = make(map[string]string)
	TsNameCount = make(map[string]int)
}
