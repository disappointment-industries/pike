package ffmpeg

import (
	"context"
	"controller/multipart"
	"fmt"
	"github.com/illarion/gonotify/v2"
	"log"
	"strings"
	"time"
)

func GoNotifyWatch(dir string, uuid, args string, params *FFmpegParams, channel chan TSChan) {
	ctx, cancel := context.WithCancel(context.Background())

	watcher, err := gonotify.NewDirWatcher(ctx, gonotify.IN_CREATE|gonotify.IN_CLOSE, dir)
	if err != nil {
		panic(err)
	}

	for {
		select {
		case event := <-watcher.C:
			//log.Printf("Event: %s\n", event)

			if event.Mask&gonotify.IN_CLOSE_WRITE != 0 {
				log.Printf("File closed: %s\n", event.Name)

				fullPathParts := strings.Split(event.Name, "/")
				fileName := fullPathParts[len(fullPathParts)-1]
				lastIndex := strings.LastIndex(fileName, ".")
				newJellyfinId := fileName[:lastIndex]

				replacedParams := strings.Replace(args, params.Id, newJellyfinId, 1)

				err := multipart.SendingMultipart(event.Name, uuid, replacedParams)
				//calcNewTsFileNames(event.Name, newJellyfinId, params)

				channel <- TSChan{
					SliceName:  event.Name,
					JellyfinId: newJellyfinId,
					Params:     params,
				}

				if err != nil {
					log.Println(err)
				}
			}

		case <-time.After(5 * time.Second):
			fmt.Println("Timeout")
			close(channel)
			cancel()
			return
		}
	}
}
