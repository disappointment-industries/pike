package ffmpeg

import (
	"errors"
	"fmt"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/pike/utils"
	"log"
	"math"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

const sliceTime float64 = 100   // 100 * hlsTime
const epsilon float64 = 60 * 10 // sec*10 TODO: test

func ParseFFmpegArgs(args []string) (*FFmpegParams, error) {
	var params FFmpegParams
	previousWasHlsTime := false
	previousWasSeekStartTime := false
	previousWasHlsFileName := false

	for _, arg := range args {
		// TODO: it's better if it is a case 💼
		if strings.Contains(arg, "file:/") {
			_, fileName, success := strings.Cut(arg, "file:")
			if !success {
				err := errors.New("couldn't parse FFmpeg arguments - filename")
				log.Println(err)
				return nil, err
			}
			lastIndex := strings.LastIndex(fileName, ".")
			params.Filename = fileName[:lastIndex]
			params.Extension = fileName[lastIndex:]
			continue
		}
		if previousWasHlsTime {
			var err error
			params.HlsTime, err = strconv.ParseFloat(arg, 64)

			if err != nil {
				return nil, err
			}

			previousWasHlsTime = false
			continue
		}
		if strings.Contains(arg, "hls_time") {
			previousWasHlsTime = true
			continue
		}
		if previousWasHlsFileName {
			params.HlsFileName = arg
			previousWasHlsFileName = false
			// DO NOT CONTINUE
		}
		if strings.Contains(arg, "hls_segment_filename") {
			previousWasHlsFileName = true
			continue
		}
		if strings.Contains(arg, "%d.ts") {
			fullPathAndUUID := strings.Split(arg, "/")
			uuidWithExtension := fullPathAndUUID[len(fullPathAndUUID)-1]
			var success bool
			params.Id, _, success = strings.Cut(uuidWithExtension, "%d")
			if !success {
				err := errors.New("couldn't parse FFmpeg arguments - Uuid with extension")
				log.Println(err)
				return nil, err
			}
			continue
		}
		if previousWasSeekStartTime {
			params.SeekStart = arg
			previousWasSeekStartTime = false
			continue
		}
		if strings.Contains(arg, "ss") {
			previousWasSeekStartTime = true
			continue
		}
		if strings.Contains(arg, ".m3u8") {
			params.M3u8 = arg
			continue
		}

	}

	return &params, nil
}

func ShouldSlice(filename string) (bool, error) {
	// ffprobe -> duration
	duration, err := getDuration(filename)
	if err != nil {
		return false, err
	}
	if *duration <= epsilon {
		return false, nil
	}
	// epsilon < duration -> slice
	return true, nil
}

func getDuration(filename string) (*float64, error) {
	// ffprobe -> duration
	return startFFprobe("-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 " + filename)
}

func startFFprobe(args string) (*float64, error) {
	argsArray := strings.Split(args, " ")
	out, err := exec.Command(env.String("FFPROBE_PATH", "/usr/bin/ffprobe"), argsArray...).Output()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	str, _, _ := strings.Cut(string(out), "\n")

	duration, err := strconv.ParseFloat(str, 64)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &duration, nil
}

func Slice(params *FFmpegParams, targetPath, commandUuid string) {
	sliceDuration := fmt.Sprintf("%f", params.HlsTime*sliceTime)

	var argsArray []string

	if len(params.SeekStart) > 0 {
		argsArray = strings.Split("-i "+params.Filename+
			params.Extension+
			" -ss "+params.SeekStart+
			" -f segment -segment_time "+sliceDuration+" -map 0 -c copy "+
			"-reset_timestamps 1 "+
			"-y "+targetPath+"/"+params.Id+"%d000000"+params.Extension, " ")
	} else {
		argsArray = strings.Split("-i "+params.Filename+
			params.Extension+
			" -f segment -segment_time "+sliceDuration+" -map 0 -c copy "+
			"-reset_timestamps 1 "+
			"-y "+targetPath+"/"+params.Id+"%d000000"+params.Extension, " ")
	}

	ProcessFFmpeg(argsArray, commandUuid)
}

func ProcessFFmpeg(params []string, commandUuid string) {
	go func() {
		_, err := utils.RunFFmpeg(params)

		if err != nil {
			log.Fatalln(err)
		}

		//TODO: check if proccess really ended and sigterm if not, maybe defer?
	}()
}

func CalcNewTsFileNamesInOrder(ch <-chan TSChan) {
	for tschan := range ch {
		calcNewTsFileNames(tschan.SliceName, tschan.JellyfinId, tschan.Params)
	}
}

func calcNewTsFileNames(sliceName, jellyfinId string, params *FFmpegParams) {
	duration, err := getDuration(sliceName)
	if err != nil {
		log.Fatalln(err)
	}
	count := int(math.Ceil(*duration / params.HlsTime)) // duration / segment time = segment number

	if _, exist := TsNameCount[params.Id]; !exist {
		TsNameCount[params.Id] = 0
	}

	tsNameCount := TsNameCount[params.Id]

	for i := 0; i < count; i++ {
		TsNameCollection[jellyfinId+strconv.Itoa(i)+".ts"] = params.Id + strconv.Itoa(tsNameCount) + ".ts"
		tsNameCount++
	}
	TsNameCount[params.Id] = tsNameCount

	log.Println("removing slice")
	err = os.Remove(sliceName)
	if err != nil {
		log.Println(err)
	}
}
