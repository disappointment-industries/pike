package main

import (
	"controller/ffmpeg"
	"controller/router"
	"gitlab.com/MikeTTh/env"
)

func main() {
	//ffmpeg.StartFFprobe("-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 /home/blint/Videos/Pirates.of.the.Caribbean.III.At.Worlds.End.2007.Open.Matte.1080p.WEB-DL.DTS.x264.HuN-TRiNiTY/p.o.t.c.at.worlds.end.om.1080p-trinity.mkv")

	ffmpeg.Init()

	//TODO: defer ffmpeg.WatchNotifyProcesses

	//args := "-analyzeduration 200M -f mov,mp4,m4a,3gp,3g2,mj2 -autorotate 0 -i file:/var/home/blint/Videos/algel_konzi_papp_laszlo_2022_04_01.mp4 -map_metadata -1 -map_chapters -1 -threads 0 -map 0:0 -map 0:1 -map -0:0 -codec:v:0 libx264 -preset v\neryfast -crf 23 -maxrate 3328675 -bufsize 6657350 -profile:v:0 high -level 41 -x264opts:0 subme=0:me_range=4:rc_lookahead=10:me=dia:no_chroma_me:8x8dct=0:partitions=none -force_key_frames:0 expr:gte(t,0+n_forced*3) -sc_threshold:v:0 0 -\nfilter_complex [0:4]scale=s=720x480:flags=fast_bilinear[sub];[0:0]setparams=color_primaries=bt709:color_trc=bt709:colorspace=bt709,scale=trunc(min(max(iw\\,ih*a)\\,min(720\\,480*a))/2)*2:trunc(min(max(iw/a\\,ih)\\,min(720/a\\,480))/2)*2,forma\nt=yuv420p[main];[main][sub]overlay=eof_action=endall:shortest=1:repeatlast=0 -start_at_zero -codec:a:0 copy -copyts -avoid_negative_ts disabled -max_muxing_queue_size 2048 -f hls -max_delay 5000000 -hls_time 3 -hls_segment_type mpegts -\nstart_number 0 -hls_segment_filename /config/transcodes/cff4ccbcfb088a85b398f1093e0b04e5%d.ts -hls_playlist_type vod -hls_list_size 0 -y /config/transcodes/cff4ccbcfb088a85b398f1093e0b04e5.m3u8"
	//argsArray := strings.Split(args, " ")
	//result, err := ffmpeg.ParseFFmpegArgs(argsArray)
	//
	//targetPath := env.String("SLICE_TARGET_FOLDER", "/tmp/pike/slices") + "/" + result.Id
	//
	//uid, _ := uuid.NewUUID()
	//
	//err = utils.CreateDirectory(targetPath)
	//if err != nil {
	//	log.Fatalln(err)
	//	return
	//}
	//
	//err = ffmpeg.Slice(result, targetPath, uid.String())
	//
	//if err != nil {
	//	log.Println(err)
	//}

	router := router.GetRouter()
	router.Run(":" + env.String("PORT", "8081"))
}
