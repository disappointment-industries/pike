# Pike

This Git project is a cloud native implementation
of an FFmpeg "wrapper "for Jellyfin, written in Go. 
This implementation is designed to allow for efficient
transcoding and streaming of media files in Jellyfin,
using FFmpeg in a containerized environment.

